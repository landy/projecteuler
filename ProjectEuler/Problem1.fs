﻿module Problem1

let multiples numbers limit =
  numbers 
  |> List.collect (fun x -> [1..limit-1] |> List.filter (fun i -> i % x = 0)) 
  |> List.distinct
  |> List.sort

let result = multiples [3;5;] 1000 |> List.sum